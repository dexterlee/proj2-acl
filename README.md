RISC-V Instruction Set Emulator
====================

The goal of this project was was to create an emulator to execute a subset of the RISC-V instruction set architecture.  


## Defining Data Types
The header files types.h contains definitions that will be referenced throughout the code.  For instance, "Register" is aliased to be of type Word (unsigned 32 bits).  The most notable data structure is the Instruction Union.  The Union datatype is used (as opposed to a struct) because the Instruction type can be one of the many defined types in the Union (and hence all types don't need to be stored).  Sizeof(Instruction) == 4 because each instruction code is 32 bits (4 bytes).  

## Utility Functions
### Instruction Parser
The parse_instruction function defined in utils.c extracts the op code and the rest of the instruction.  It then returns the instruction.

```
//c

Instruction parse_instruction(uint32_t instruction_bits) {
    Instruction instruction;
    /* YOUR CODE HERE */
    instruction.opcode = instruction_bits & 127;
    instruction.rest = instruction_bits >> 7;
    return instruction;

```

### Sign-Extension
The sign_extend_number function sign extends the given field to a 32-bit integer.
```
//c
int sign_extend_number( unsigned int field, unsigned int n) {
    /* YOUR CODE HERE */
    signed int signed_field = (signed int)field;
    int offset = ((sizeof(unsigned int))*8)-n;
    return (signed_field << offset) >> offset;
}
```
The original number is passed as the field variable, and n stores how many bits the original number has.  We first cast field as a 32 bit signed integer.  This preserves the original data but informs the compiler that the MSB is a sign bit.  Then we determine how much to shift by taking the difference between 32 and n.  Shifting to the left by this difference will bring the original number to the MSB position.  Shifting this result to the right will shift the number back to its original position and also sign extend because of the signed int casting we perfomred earlier.  


## Decoding Instructions
Instructions are decoded by first calling the parse_instruction function on the instruction bits.  A case statement filters the instruction by op code.