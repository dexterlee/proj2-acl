#include <stdio.h> // for stderr
#include <stdlib.h> // for exit()
#include "types.h"
#include "utils.h"
#include "riscv.h"

void execute_rtype(Instruction, Processor *);
void execute_itype_except_load(Instruction, Processor *);
void execute_branch(Instruction, Processor *);
void execute_jal(Instruction, Processor *);
void execute_jalr(Instruction, Processor *);
void execute_load(Instruction, Processor *, Byte *);
void execute_store(Instruction, Processor *, Byte *);
void execute_ecall(Processor *, Byte *);
void execute_lui(Instruction, Processor *);
void execute_auipc(Instruction, Processor *);

void execute_instruction(uint32_t instruction_bits, Processor *processor,Byte *memory) {
    Instruction instruction = parse_instruction(instruction_bits); // Look in utils.c
    switch(instruction.opcode) {
        case 0x33:
            /* YOUR CODE HERE */
						execute_rtype(instruction, processor);
            break;
        case 0x13:
            /* YOUR CODE HERE */
						execute_itype_except_load(instruction, processor);
            break;
        case 0x3:
            /* YOUR CODE HERE */
						execute_load(instruction, processor, memory);
            break;
        case 0x67:
            /* YOUR CODE HERE */
						execute_jalr(instruction, processor);
            break;
        case 0x23:
            /* YOUR CODE HERE */
						execute_store(instruction, processor, memory);
            break;
        case 0x63:
            /* YOUR CODE HERE */
						execute_branch(instruction, processor);
            break;
        case 0x37:
            /* YOUR CODE HERE */
						execute_lui(instruction, processor);
            break;
        case 0x17:
            /* YOUR CODE HERE */
						execute_auipc(instruction, processor);
            break;
        case 0x6F:
            /* YOUR CODE HERE */
						execute_jal(instruction, processor);
            break;
        case 0x73:
            /* YOUR CODE HERE */
						execute_ecall(processor, memory);
            break;
        default: // undefined opcode
            handle_invalid_instruction(instruction);
            break;
    }
}

void execute_rtype(Instruction instruction, Processor *processor) {
		unsigned int rs1 = instruction.rtype.rs1;
		unsigned int rs2 = instruction.rtype.rs2;
		unsigned int rd = instruction.rtype.rd;
		Word uRrs1 = (processor->R[rs1]);
		Word uRrs2 = (processor->R[rs2]);
		sWord Rrs1 = (sWord)(processor->R[rs1]);
		sWord Rrs2 = (sWord)(processor->R[rs2]);
    switch (instruction.rtype.funct3){
        case 0x0:
            /* YOUR CODE HERE */
						if (instruction.rtype.funct7 == 0x00) { //add
							processor->R[rd] = Rrs1 + Rrs2;
						}
						else if (instruction.rtype.funct7 == 0x01) { //mul
							processor->R[rd] = (Rrs1*Rrs1) & 0xFFFFFFFF;

						}
						else { //sub
							processor->R[rd] = Rrs1 - Rrs2;
						}
            break;
        case 0x1:
            /* YOUR CODE HERE */
						if (instruction.rtype.funct7 == 0x00) { //sll
								processor->R[rd] = Rrs1 << (0x1f & Rrs2);
						}
						else { //mulh
							processor->R[rd] = (Rrs1*Rrs2) & 0xFFFFFFFF00000000;

						}
            break;
        case 0x2: //slt
						if (Rrs1 < Rrs2) {
							processor->R[rd] = 1;
						}
						else {
							processor->R[rd] = 0;
						}
            /* YOUR CODE HERE */
            break;
        case 0x3:
            /* YOUR CODE HERE */
						if (instruction.rtype.funct7 == 0x00) { //sltu
							//unsigned int rs1_val = processor->R[rs1];
							//unsigned int rs2_val = processor->R[rs2];
							if (uRrs1 < uRrs2) {
								processor->R[rd] = 1;
							}
							else {
								processor->R[rd] = 0;
							}
						}
						else { //mulhu
							//unsigned int Rrs1 = processor->R[rs1];
							//unsigned int Rrs2 = processor->R[rs2];
							processor->R[rd] = (uRrs1*uRrs2) & 0xFFFFFFFF00000000;

						}
            break;
        case 0x4:
            /* YOUR CODE HERE */
						if (instruction.rtype.funct7 == 0x00) { //xor
							processor->R[rd] = Rrs1 ^ Rrs2;
						}
						else { //div
							processor->R[rd] = Rrs1 / Rrs2;
						}
            break;
        case 0x5:
            /* YOUR CODE HERE */
						if (instruction.rtype.funct7 == 0x00) { //srl
              processor->R[rd] = uRrs1 >> (0x1f & Rrs2);
            }
            else if (instruction.rtype.funct7 == 0x01) { //divu
							//unsigned int Rrs1 = processor->R[rs1];
							//unsigned int Rrs2 = processor->R[rs2];
							processor->R[rd] = (uRrs1) / (uRrs2);

            }
            else { //sra
              processor->R[rd] = Rrs1 >> (0x1f & Rrs2);
            }
            break;
        case 0x6:
            /* YOUR CODE HERE */
						if (instruction.rtype.funct7 == 0x00) { //or
							processor->R[rd] = Rrs1 | Rrs2;
						}
						else { //rem
							processor->R[rd] = Rrs1 % Rrs2;

						}
            break;
        case 0x7:
            /* YOUR CODE HERE */
						if (instruction.rtype.funct7 == 0x00) { //and
							processor->R[rd] = Rrs1 & Rrs2;
						}
						else { //remu
							//unsigned int Rrs1 = processor->R[rs1];
							//unsigned int Rrs2 = processor->R[rs2];
							processor->R[rd] = uRrs1 % uRrs2;
						}
            break;
        default:
            handle_invalid_instruction(instruction);
            exit(-1);
            break;
    }
		processor->PC = (processor->PC) + 4;
}

void execute_itype_except_load(Instruction instruction, Processor *processor) {
		unsigned int rs1 = instruction.rtype.rs1;
		int sign_imm = sign_extend_number(instruction.itype.imm, 12);
		unsigned int rd = instruction.rtype.rd;
		Word uRrs1 = (processor->R[rs1]);
		sWord Rrs1 = (sWord)(processor->R[rs1]);
		switch (instruction.itype.funct3) {
        case 0x0:
            /* YOUR CODE HERE */
						if (instruction.itype.opcode == 0x13) { //addi
							processor->R[rd] = Rrs1 + sign_imm;
						}
						else { //jalr
							processor->R[rd] = processor->PC + 0b100000;
							processor->PC = Rrs1 + sign_imm;
						}

            break;
        case 0x1: //slli
            /* YOUR CODE HERE */
						processor->R[rd] = Rrs1 << ((0x1f) & sign_imm);
            break;
        case 0x2://slti
            /* YOUR CODE HERE */
						if (Rrs1 < sign_imm) {
							processor->R[rd] = 1;
						}
						else {
							processor->R[rd] = 0;
						}
            break;
        case 0x3: //sltiu
            /* YOUR CODE HERE */
						//unsigned int Rrs1 = processor->R[rs1];
						if (uRrs1 < instruction.itype.imm) {
							processor->R[rd] = 1;
						}
						else {
							processor->R[rd] = 0;
						}

            break;
        case 0x4: //xori
            /* YOUR CODE HERE */
						processor->R[rd] = Rrs1 ^ sign_imm;
            break;
        case 0x5:
            /* YOUR CODE HERE */
						if (instruction.itype.imm >> 5 == 0x00) { //srli
							processor->R[rd] = uRrs1 >> (0x1f & sign_imm);
						}
						else { //srai
							processor->R[rd] = Rrs1 >> (0x1f & sign_imm);
						}

            break;
        case 0x6:
            /* YOUR CODE HERE */
						processor->R[rd] = Rrs1 | sign_imm;
            break;
        case 0x7:
            /* YOUR CODE HERE */
						processor->R[rd] = Rrs1 & sign_imm;
            break;
        default:
            handle_invalid_instruction(instruction);
            break;
    }
		processor->PC = (processor->PC) + 4;
}

void execute_ecall(Processor *p, Byte *memory) {

    Register i;

    // What do we switch on?
    switch(p->R[10]) { //change this later
        case 1: // print an integer
            printf("%d",p->R[11]);
            break;
        case 4: // print a string
            for(i = p->R[11]; i < MEMORY_SPACE && load(memory, i, LENGTH_BYTE); i++) {
                printf("%c",load(memory,i,LENGTH_BYTE));
            }
            break;
        case 10: // exit
            printf("exiting the simulator\n");
            exit(0);
            break;
        case 11: // print a character
            printf("%c",p->R[11]);
            break;
        default: // undefined ecall
            printf("Illegal ecall number %d\n", p->R[10]);
            exit(-1);
            break;
    }
		p->PC = (p->PC) + 4;
}

void execute_branch(Instruction instruction, Processor *processor) {
		unsigned int rs1 = instruction.rtype.rs1;
		unsigned int rs2 = instruction.rtype.rs2;
		//unsigned int rd = instruction.rtype.rd;
		Word uRrs1 = (processor->R[rs1]);
		Word uRrs2 = (processor->R[rs2]);
		sWord Rrs1 = (sWord)(processor->R[rs1]);
		sWord Rrs2 = (sWord)(processor->R[rs2]);
		switch (instruction.sbtype.funct3) {
        case 0x0:
            /* YOUR CODE HERE */
						if (Rrs1 == Rrs2) {
							processor->PC = processor->PC + get_branch_offset(instruction);
						}

						else {
							processor->PC = (processor->PC) + 4;
						}
            break;
        case 0x1:
            /* YOUR CODE HERE */
						if (Rrs1 != Rrs2) {
							processor->PC = processor->PC + get_branch_offset(instruction);
						}
						else {
							processor->PC = (processor->PC) + 4;
						}
            break;
        case 0x4:
            /* YOUR CODE HERE */
						if (Rrs1 < Rrs2) {
							processor->PC = processor->PC + get_branch_offset(instruction);
						}
						else {
							processor->PC = (processor->PC) + 4;
						}
            break;
        case 0x5:
            /* YOUR CODE HERE */
						if (Rrs1 >= Rrs2) {
							processor->PC = processor->PC + get_branch_offset(instruction);
						}
						else {
							processor->PC = (processor->PC) + 4;
						}
            break;
        case 0x6:
            /* YOUR CODE HERE */
						if (uRrs1 < uRrs2) {
							processor->PC = processor->PC + get_branch_offset(instruction);
						}
						else {
							processor->PC = (processor->PC) + 4;
						}
            break;
        case 0x7:
            /* YOUR CODE HERE */
						if (uRrs1 >= uRrs2) {
							processor->PC = processor->PC + get_branch_offset(instruction);
						}
						else {
							processor->PC = (processor->PC) + 4;
						}
            break;
        default:
            handle_invalid_instruction(instruction);
            exit(-1);
            break;
    }

}

void execute_load(Instruction instruction, Processor *processor, Byte *memory) {
		unsigned int rs1 = instruction.rtype.rs1;
		unsigned int rd = instruction.rtype.rd;
		Word uRrs1 = (processor->R[rs1]);
		//sWord Rrs1 = (sWord)(processor->R[rs1]);
		int imm = sign_extend_number(instruction.itype.imm, 12);
    switch (instruction.itype.funct3) {
        case 0x0: //lb
            /* YOUR CODE HERE */
						processor->R[rd] = sign_extend_number(load(memory, uRrs1 + imm, LENGTH_BYTE), 8*(LENGTH_BYTE));
            break;
        case 0x1: //lh
            /* YOUR CODE HERE */
						processor->R[rd] = sign_extend_number(load(memory, uRrs1 + imm, LENGTH_HALF_WORD), 8*(LENGTH_HALF_WORD));
            break;
        case 0x2: //lw
            /* YOUR CODE HERE */
						processor->R[rd] = sign_extend_number(load(memory, uRrs1 + imm, LENGTH_WORD), 8*(LENGTH_WORD));
            break;
        case 0x4: //lbu
            /* YOUR CODE HERE */
						processor->R[rd] = (Word)load(memory, uRrs1 + imm, LENGTH_BYTE);
            break;
        case 0x5: //lhu
            /* YOUR CODE HERE */
						processor->R[rd] = (Word)load(memory, uRrs1 + imm, LENGTH_HALF_WORD);
            break;
        default:
            handle_invalid_instruction(instruction);
            break;
    }
		processor->PC = (processor->PC) + 4;
}

void execute_store(Instruction instruction, Processor *processor, Byte *memory) {
		unsigned int rs1 = instruction.stype.rs1;
		unsigned int rs2 = instruction.stype.rs2;
		Word uRrs1 = (processor->R[rs1]);
		Word uRrs2 = (processor->R[rs2]);
		//sWord Rrs1 = (sWord)(processor->R[rs1]);
		//sWord Rrs2 = (sWord)(processor->R[rs2]);
		int imm = get_store_offset(instruction);
		switch (instruction.stype.funct3) {
        case 0x0: //sb
            /* YOUR CODE HERE */
						store(memory, (Address)(uRrs1+imm), LENGTH_BYTE, uRrs2);
            break;
        case 0x1: //sh
            /* YOUR CODE HERE */
						store(memory, uRrs1+imm, LENGTH_HALF_WORD, uRrs2);
            break;
        case 0x2: //sw
            /* YOUR CODE HERE */
						store(memory, uRrs1+imm, LENGTH_WORD, uRrs2);
            break;
        default:
            handle_invalid_instruction(instruction);
            exit(-1);
            break;
    }
		processor->PC = (processor->PC) + 4;
}

void execute_jal(Instruction instruction, Processor *processor) {
    /* YOUR CODE HERE */
		unsigned int rd = instruction.ujtype.rd;
		//int imm = sign_extend_number((instruction.ujtype.imm), 20);
		int imm = get_jump_offset(instruction);
		processor->R[rd] = (processor->PC) + 4;
		processor->PC = (processor->PC) + imm;
}

void execute_jalr(Instruction instruction, Processor *processor) {
    /* YOUR CODE HERE */
		unsigned int rd = instruction.itype.rd;
		unsigned int rs1 = instruction.itype.rs1;
		Word uRrs1 = (processor->R[rs1]);
		//sWord Rrs1 = (sWord)(processor->R[rs1]);
		int imm = sign_extend_number(instruction.itype.imm, 12);

		processor->R[rd] = (processor->PC) + 4;
		processor->PC = uRrs1 + imm;

}

void execute_lui(Instruction instruction, Processor *processor) {
    /* YOUR CODE HERE */
		unsigned int rd = instruction.rtype.rd;
		//unsigned int rs1 = instruction.rtype.rs1;
		//Word uRrs1 = (processor->R[rs1]);
		int imm = (instruction.utype.imm);

		processor->R[rd] = imm << 12;
		processor->PC = (processor->PC) + 4;


}

void execute_auipc(Instruction instruction, Processor *processor) {
    /* YOUR CODE HERE */
		unsigned int rd = instruction.rtype.rd;
		//unsigned int rs1 = instruction.rtype.rs1;
		//Word uRrs1 = (processor->R[rs1]);
		int imm = instruction.utype.imm;

		processor->R[rd] = processor->PC + (imm << 12);
		processor->PC = (processor->PC) + 4;


}

void store(Byte *memory, Address address, Alignment alignment, Word value) {
    /* YOUR CODE HERE */
		int i = 0;
		Word mask = 0x000000FF;
		while (i < alignment) {
			memory[address + i] = (Byte)((mask & value) >> (8*i));  //memory only stores 1 byte
			mask <<= 8;
			i++;


		}
}

Word load(Byte *memory, Address address, Alignment alignment) {
    /* YOUR CODE HERE */
		int i = 0;
		Word a = 0; //32 bit unsigned
		while (i < alignment) {
			a += ((Word)(memory[address + i]) << 8*i);
			i++;
		}
		return a;
    //return sign_extend_number(a, 8*i);
}
