#include "utils.h"
#include <stdio.h>
#include <stdlib.h>

/* Sign extends the given field to a 32-bit integer where field is
 * interpreted an n-bit integer. Look in test_utils.c for examples. */
int sign_extend_number( unsigned int field, unsigned int n) {
    /* YOUR CODE HERE */
    signed int signed_field = (signed int)field;
    int offset = ((sizeof(unsigned int))*8)-n;
    return (signed_field << offset) >> offset;

    /*
    int signed_bit = field>>(n-1);
    signed_bit&()
    if (signed_bit == 1) {
      //negative case
      return (int32_t)field
    }
    else { //positive case
      return (uint32_t)field

    }
  }
  */

    //return 0;
}

/* Unpacks the 32-bit machine code instruction given into the correct
 * type within the instruction struct. Look at types.h */
Instruction parse_instruction(uint32_t instruction_bits) {
    Instruction instruction;
    /* YOUR CODE HERE */
    instruction.opcode = instruction_bits & 127;
    instruction.rest = instruction_bits >> 7;
    return instruction;
}

/* Return the number of bytes (from the current PC) to the branch label using the given
 * branch instruction */
int get_branch_offset(Instruction instruction) {
    /* YOUR CODE HERE */
    int four_to_one = ((instruction.sbtype.imm5)&(0b11110))>>1;
    int eleven = ((instruction.sbtype.imm5)&(0b00001))<<10;
    int twelve = ((instruction.sbtype.imm7)&(0b1000000)) << 5;
    int ten_to_five = ((instruction.sbtype.imm7)&(0b0111111)) << 4;
    int sum = (four_to_one+eleven+twelve+ten_to_five) << 1;
    return sign_extend_number(sum, 13);
}

/* Returns the number of bytes (from the current PC) to the jump label using the given
 * jump instruction */
int get_jump_offset(Instruction instruction) {
    /* YOUR CODE HERE */
    int nineteen_to_twelve = ((instruction.ujtype.imm)&(0b11111111))<<11;
    int eleven = ((instruction.ujtype.imm)&(0b100000000))<<2;
    int ten_to_one = ((instruction.ujtype.imm)&(0b1111111111000000000))>>9;
    int twenty = ((instruction.ujtype.imm)&(0b10000000000000000000));
    int sum = (nineteen_to_twelve+eleven+ten_to_one+twenty)<<1;
    return sign_extend_number(sum, 21);
}

/* Returns the byte offset (from the address in rs2) for storing info using the given
 * store instruction */
int get_store_offset(Instruction instruction) {
    /* YOUR CODE HERE */
    int four_to_zero = (instruction.stype.imm5);
    int eleven_to_five = (instruction.stype.imm7) << 5;
    int sum = (four_to_zero+eleven_to_five);
    return sign_extend_number(sum,12);
}

void handle_invalid_instruction(Instruction instruction) {
    printf("Invalid Instruction: 0x%08x\n", instruction.bits); 
    exit(-1);
}

void handle_invalid_read(Address address) {
    printf("Bad Read. Address: 0x%08x\n", address);
    exit(-1);
}

void handle_invalid_write(Address address) {
    printf("Bad Write. Address: 0x%08x\n", address);
    exit(-1);
}
