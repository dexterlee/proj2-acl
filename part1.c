#include <stdio.h> // for stderr
#include <stdlib.h> // for exit()
#include "types.h"
#include "utils.h"

void print_rtype(char *, Instruction);
void print_itype_except_load(char *, Instruction, int);
void print_load(char *, Instruction);
void print_store(char *, Instruction);
void print_branch(char *, Instruction);
void print_utype(char*, Instruction);
void print_jal(Instruction);
void print_ecall(Instruction);
void write_rtype(Instruction);
void write_itype_except_load(Instruction);
void write_load(Instruction);
void write_store(Instruction);
void write_branch(Instruction);


void decode_instruction(uint32_t instruction_bits) {
    Instruction instruction = parse_instruction(instruction_bits); // Look in utils.c
    switch(instruction.opcode) {
        char *name;
        case 0x33: //R type
            /* YOUR CODE HERE */
            /*
            instruction.rtype.rd = (instruction.rest) & 0x1F;
            instruction.rtype.funct3 = ((instruction.rest) >>5) & 0x7
            instruction.rtype.rs1 = ((instruction.rest) >> 8) & 0x1F;
            instruction.rtype.rs2 = ((instruction.rest) >>13 ) & 0x1F;
            instruction.rtype.funct7 = ((instruction.rest) >>18 ) & 0x7F;
            */
            write_rtype(instruction);
            break;
        case 0x13:
            /* YOUR CODE HERE */
            write_itype_except_load(instruction);
            break;
        case 0x3:
            /* YOUR CODE HERE */
            write_load(instruction);
            break;
        case 0x67: //jalr (I-format)
            /* YOUR CODE HERE */
            write_itype_except_load(instruction);
            break;
        case 0x23:
            /* YOUR CODE HERE */
            write_store(instruction);
            break;
        case 0x63:
            /* YOUR CODE HERE */
            write_branch(instruction);
            break;
        case 0x37: //U-format (e.g. lui)
            /* YOUR CODE HERE */
            name = "lui";
            print_utype(name, instruction);
            break;
        case 0x17: //U-Format (e.g. auipic)
            /* YOUR CODE HERE */
            name = "auipc";
            print_utype(name, instruction);
            break;
        case 0x6F: //UJ-Format (e.g. jal)
            /* YOUR CODE HERE */
            print_jal(instruction);
            break;
        case 0x73: //I-Format (e.g. CSRRW)
            /* YOUR CODE HERE */
            print_ecall(instruction);
            break;
        default: // undefined opcode
            handle_invalid_instruction(instruction);
            break;
    }
}

void write_rtype(Instruction instruction) {
    /* HINT: Hmmm, it's seems that there's way more R-Type instructions that funct3 possibilities... */
    switch (instruction.rtype.funct3) {
        char* name;
        case 0x0:
            /* YOUR CODE HERE */
            if (instruction.rtype.funct7 == 0x00) {
              name = "add";
            }
            else if (instruction.rtype.funct7 == 0x01) {
              name = "mul";
            }
            else {
              name = "sub";
            }
            print_rtype(name, instruction);
            break;
        case 0x1:
            /* YOUR CODE HERE */
            if (instruction.rtype.funct7 == 0x00) {
              name = "sll";
            }
            else {
              name = "mulh";
            }
            print_rtype(name, instruction);
            break;
        case 0x2:
            /* YOUR CODE HERE */
            name = "slt";
            print_rtype(name, instruction);
            break;
        case 0x3:
            /* YOUR CODE HERE */
            if (instruction.rtype.funct7 == 0x00) {
            name = "sltu";
            }
            else {
              name = "mulhu";
            }
            print_rtype(name, instruction);
            break;
        case 0x4:
            /* YOUR CODE HERE */
            if (instruction.rtype.funct7 == 0x00) {
              name = "xor";
            }
            else {
              name = "div";
            }
            print_rtype(name, instruction);
            break;
        case 0x5:
            /* YOUR CODE HERE */
            if (instruction.rtype.funct7 == 0x00) {
              name = "srl";
            }
            else if (instruction.rtype.funct7 == 0x01) {
              name = "divu";
            }
            else {
              name = "sra";
            }
            print_rtype(name, instruction);
            break;
        case 0x6:
            /* YOUR CODE HERE */
            if (instruction.rtype.funct7 == 0x00) {
            name = "or";
            }
            else {
              name = "rem";
            }

            print_rtype(name, instruction);
            break;
        case 0x7:
            /* YOUR CODE HERE */
            if (instruction.rtype.funct7 == 0x00) {
              name = "and";
            }
            else {
              name = "remu";
            }
            print_rtype(name, instruction);
            break;
        default:
            handle_invalid_instruction(instruction);
            break;
    }
}

void write_itype_except_load(Instruction instruction) {
    switch (instruction.itype.funct3) {
        char *name;
        case 0x0:
            /* YOUR CODE HERE */
            if (instruction.itype.opcode == 0x13) {
              name = "addi";
            }
            else {
              name = "jalr";
            }


            print_itype_except_load(name, instruction, instruction.itype.imm);
            break;
        case 0x1:
            /* YOUR CODE HERE */
            name = "slli";
            print_itype_except_load(name, instruction, instruction.itype.imm);
            break;
        case 0x2:
            /* YOUR CODE HERE */
            name = "slti";
            print_itype_except_load(name, instruction, instruction.itype.imm);
            break;
        case 0x3:
            /* YOUR CODE HERE */
            name = "sltiu";
            print_itype_except_load(name, instruction, instruction.itype.imm);
            break;
        case 0x4:
            /* YOUR CODE HERE */
            name = "xori";
            print_itype_except_load(name, instruction, instruction.itype.imm);
            break;
        case 0x5:
            /* HINT: What makes the immediate here special? */
            /* YOUR CODE HERE */
            //unsigned int funct7 = instruction.itype.imm >> 5;
            //unsigned int immediate = instruction.itype.imm & 0b000000011111;
            if (instruction.itype.imm >> 5 == 0x00) {
              name = "srli";
            }
            else {
              name = "srai";
            }
            printf("%s\tx%d, x%d, %d\n", name, instruction.rtype.rd, instruction.rtype.rs1, instruction.itype.imm & 0b000000011111);
            //print_itype_except_load(name, instruction, instruction.itype.imm);
            break;
        case 0x6:
            /* YOUR CODE HERE */
            name = "ori";
            print_itype_except_load(name, instruction, instruction.itype.imm);
            break;
        case 0x7:
            /* YOUR CODE HERE */
            name = "andi";
            print_itype_except_load(name, instruction, instruction.itype.imm);
            break;
        default:
            handle_invalid_instruction(instruction);
            break;
    }
}

void write_load(Instruction instruction) {
    switch (instruction.itype.funct3) {
        char *name;
        case 0x0:
            /* YOUR CODE HERE */
            name = "lb";
            print_load(name, instruction);
            break;
        case 0x1:
            /* YOUR CODE HERE */
            name = "lh";
            print_load(name, instruction);
            break;
        case 0x2:
            /* YOUR CODE HERE */
            name = "lw";
            print_load(name, instruction);
            break;
        case 0x4:
            /* YOUR CODE HERE */
            name = "lbu";
            print_load(name, instruction);
            break;
        case 0x5:
            /* YOUR CODE HERE */
            name = "lhu";
            print_load(name, instruction);
            break;
        default:
            handle_invalid_instruction(instruction);
            break;
    }
}

void write_store(Instruction instruction) {
    switch (instruction.stype.funct3) {
        char *name;
        case 0x0:
            /* YOUR CODE HERE */
            name = "sb";
            print_store(name, instruction);
            break;
        case 0x1:
            /* YOUR CODE HERE */
            name = "sh";
            print_store(name, instruction);
            break;
        case 0x2:
            /* YOUR CODE HERE */
            name = "sw";
            print_store(name, instruction);
            break;
        default:
            handle_invalid_instruction(instruction);
            break;
    }
}

void write_branch(Instruction instruction) {
    switch (instruction.sbtype.funct3) {
      char *name;
        case 0x0:
            /* YOUR CODE HERE */
            name = "beq";
            print_branch(name, instruction);
            break;
        case 0x1:
            /* YOUR CODE HERE */
            name = "bne";
            print_branch(name, instruction);
            break;
        case 0x4:
            /* YOUR CODE HERE */
            name = "blt";
            print_branch(name, instruction);
            break;
        case 0x5:
            /* YOUR CODE HERE */
            name = "bge";
            print_branch(name, instruction);
            break;
        case 0x6:
            /* YOUR CODE HERE */
            name = "bltu";
            print_branch(name, instruction);
            break;
        case 0x7:
            /* YOUR CODE HERE */
            name = "bgeu";
            print_branch(name, instruction);
            break;
        default:
            handle_invalid_instruction(instruction);
            break;
    }
}

/* utils.c and utils.h might be useful here... */

void print_utype(char* name, Instruction instruction) {
    /* YOUR CODE HERE */
    printf(UTYPE_FORMAT, name, instruction.utype.rd, (instruction.utype.imm));
}

void print_jal(Instruction instruction) {
    /* YOUR CODE HERE */
    printf(JAL_FORMAT, instruction.ujtype.rd, get_jump_offset(instruction));
}

void print_ecall(Instruction instruction) {
    /* YOUR CODE HERE */
    printf(ECALL_FORMAT);
}

void print_rtype(char *name, Instruction instruction) {
    /* YOUR CODE HERE */
    printf(RTYPE_FORMAT, name, instruction.rtype.rd, instruction.rtype.rs1, instruction.rtype.rs2);
}

void print_itype_except_load(char *name, Instruction instruction, int imm) {
    /* YOUR CODE HERE */
    int sign_imm = sign_extend_number(instruction.itype.imm, 12);
    printf(ITYPE_FORMAT, name, instruction.itype.rd, instruction.itype.rs1, sign_imm);
}

void print_load(char *name, Instruction instruction) {
    /* YOUR CODE HERE */
    printf(MEM_FORMAT, name, instruction.itype.rd, sign_extend_number(instruction.itype.imm, 12), instruction.itype.rs1);
}

void print_store(char *name, Instruction instruction) {
    /* YOUR CODE HERE */
    printf(MEM_FORMAT, name, instruction.stype.rs2, get_store_offset(instruction), instruction.stype.rs1);
}

void print_branch(char *name, Instruction instruction) {
    /* YOUR CODE HERE */
    printf(BRANCH_FORMAT, name, instruction.sbtype.rs1, instruction.sbtype.rs2, get_branch_offset(instruction));
}
